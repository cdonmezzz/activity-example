import React, { useState } from "react";
import womanImg from "../../assets/woman.svg";
import manImg from "../../assets/man.svg";
import { CircularProgressbar } from "react-circular-progressbar";
import Slider from "react-rangeslider";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";

function Ratelt() {
  const [volume, setVolume] = useState(4);
  const percentage = 30;
  const until = 100 - percentage;
  return (
    <section className="rate-it">
      <div className="container">
        <div className="row">
          <div className="col-md-4">
            <div className="content">
              <div className="card left">
                <img src={womanImg} alt="" />
                <div className="card-title">
                  <h4 className="title bold-mini">
                    Prof. Dr. Margaret Hampton
                  </h4>
                  <p className="title normal-mini">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                    ut tincidunt augue. Donec auctor vehicula elit, elementum
                    efficitur dolor pulvinar quis.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="content pb">
              <CircularProgressbar
                value={percentage}
                strokeWidth={7}
                text={`%${percentage}`}
              />
              <div className="vs">VS</div>
              <div className="until">%{until}</div>
            </div>
            <div className="content">
              <p className="title rateit">
                Hangi yaklaşıma <br /> <span>daha çok</span> katılıyorsunuz?
              </p>
            </div>
            <div className="content">
              <div className="slider-text">
                <span>{volume}</span>/10
              </div>
              <div className="range-slider">
                <div className="range-slider-lines">
                  {[...new Array(11)].map((s, i) => (
                    <span key={i} />
                  ))}
                </div>
                <Slider
                  value={volume}
                  onChange={(value) => setVolume(value)}
                  max={10}
                  tooltip={false}
                />
              </div>
              <div className="slider-text">
                <span>{10 - volume}</span>/10
              </div>
            </div>
            <div className="push-btn">
              <button className="push">
                OYLA
                <FontAwesomeIcon icon={faPlus} />
              </button>
            </div>
          </div>
          <div className="col-md-4">
            <div className="content">
              <div className="card right">
                <img src={manImg} alt="" />
                <div className="card-title">
                  <h4 className="title bold-mini">Prof. Dr. John Doe</h4>
                  <p className="title normal-mini">
                    Quisque vitae dui in mi auctor placerat ut vel ipsum. Etiam
                    sodales fringilla elementum. Vestibulum tempor urna sed
                    mattis congue.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Ratelt;
