import React from "react";
import Brand from "../../assets/brand.svg";
import LogoH from "../../assets/logo-r.png";

function index() {
  return (
    <header className="d-flex">
      <div className="content d-flex align-items-center justify-content-between">
        <a className="brand" href="https://www.google.com">
          <img src={Brand} alt="" />
        </a>
        <div className="navbar">
          <ul className="d-flex align-items-center">
            <li>
              <a className="link" href="https://www.google.com">
                Etkinlik Hakkında
              </a>
            </li>
            <li>
              <a className="link" href="https://www.google.com">
                Konuşmacılar
              </a>
            </li>
            <li>
              <a className="link" href="https://www.google.com">
                Program
              </a>
            </li>
            <li>
              <a className="link" href="https://www.google.com">
                Soru / Yorum
              </a>
            </li>
            <button className="btn link">Hesabım</button>
          </ul>
        </div>
        <a className="logo" href="https://www.google.com">
          <img src={LogoH} className="logo-r" alt="" />
        </a>
      </div>
    </header>
  );
}

export default index;
