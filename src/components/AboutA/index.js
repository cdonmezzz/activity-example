import React from "react";

function index() {
  return (
    <section className="about-a">
      <div className="container d-flex align-items-center">
        <div className="row d-flex align-items-center">
          <div className="left-img"></div>
          <div className="col-lg-6 offset-lg-6">
            <div className="right-activity d-flex align-items-center justify-content-between">
              <div className="a-left">
                <h2 className="title">Etkinlik Hakkında</h2>
                <p className="title normal-mini size-mini">
                  Quisque vitae dui in mi auctor placerat ut vel ipsum.
                  Pellentesque id consequat magna, et maximus est. Etiam sodales
                  fringilla elementum. Vestibulum tempor urna sed mattis congue.
                </p>
              </div>
              <button className="btn style-add">Ayrıntılı Bilgi</button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default index;
