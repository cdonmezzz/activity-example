import React from "react";
import logoF from "../../assets/f-logo.svg";
import logoFF from "../../assets/f-logo2.svg";

function index() {
  return (
    <footer>
      <div className="container d-flex  align-items-center justify-content-between">
        <div className="d-flex align-items-center">
          <p className="information">
            © 2020 - Information contained in this website does not substitute
            for consulting a healthcare professional.
          </p>

          <div className="footer-menu">
            <ul>
              <li>
                <a className="link f-link mr-20" href="https://www.google.com">
                  Legal
                </a>
              </li>
              <li>
                <a className="link f-link" href="https://www.google.com">
                  Privacy Policy
                </a>
              </li>
            </ul>
          </div>
        </div>

        <div className="f-logos d-flex align-items-center">
          <p>Powered by</p>
          <img src={logoF} alt="" />
          <img src={logoFF} alt="" />
        </div>
      </div>
    </footer>
  );
}

export default index;
