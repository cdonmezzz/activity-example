import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

function index() {
  return (
    <section className="question-comment">
      <div className="container">
        <div className="row">
          <div className="mmx">
            <div className="q-c">
              <h2 className="title">Soru / Yorum</h2>
              <p className="title normal-mini size-mini">
                Quisque vitae dui in mi auctor placerat ut vel ipsum. Etiam
                sodales fringilla elementum. Vestibulum tempor urna.
              </p>
            </div>
          </div>
        </div>
        <div className="posts">
          <div className="add-post">
            <p className="title min">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
              accumsan mi finibus, consectetur ligula gravida, vestibulum magna.
              Duis elit diam, tristique vitae libero sit amet?
            </p>
            <button className="btn post">Gönder</button>
          </div>

          <h5>Sorularınız</h5>
          <div className="all-post">
            <div className="item">
              <p className="number">1</p>
              <p className="description title min xmin">
                Quisque lobortis, tortor at vehicula commodo, urna nisi congue
                orci, non gravida ante felis id lacus?
              </p>

              <button className="close">
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
            <div className="item">
              <p className="number">2</p>
              <p className="description title min xmin">
                Aenean a ipsum at quam blandit interdum sit amet nec nisl.
                Phasellus et elit quam?
              </p>

              <button className="close">
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
            <div className="item">
              <p className="number">3</p>
              <p className="description title min xmin">
                Aliquam in metus in est mattis pharetra. Vestibulum tincidunt
                nibh ut urna porttitor?
              </p>

              <button className="close">
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
            <div className="item">
              <p className="number">4</p>
              <p className="description title min xmin">
                Nunc lacinia mattis purus a tempor. Donec convallis nisl in
                metus ultricies, nec luctus justo tincidunt?
              </p>

              <button className="close">
                <FontAwesomeIcon icon={faTimes} />
              </button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default index;
