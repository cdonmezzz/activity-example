import React from "react";

import { CountdownCircleTimer } from "react-countdown-circle-timer";
import "moment/locale/tr";
import Clock from "react-live-clock";

import "moment-timezone";
import "react-moment";
const minuteSeconds = 60;
const hourSeconds = minuteSeconds * 60;
const daySeconds = hourSeconds * 24;

const timerProps = {
  isPlaying: true,
  size: 90,
  strokeWidth: 5,
};

const renderTime = (time) => {
  return (
    <div className="time-wrapper">
      <div className="title time">{time}</div>
    </div>
  );
};

const getTimeSeconds = (time) => Math.ceil(minuteSeconds - time) - 1;
const getTimeMinutes = (time) => Math.ceil(time / minuteSeconds) - 1;
const getTimeHours = (time) => Math.ceil(time / hourSeconds) - 1;
const getTimeDays = (time) => Math.ceil(time / daySeconds) - 1;

export default function CountDown() {
  // const liveTime = new Date().toLocaleString();

  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     console.log("This will run every second!");
  //   }, 1000);
  //   return () => clearInterval(interval);
  // }, []);

  const startTime = Date.now() / 1000; // use UNIX timestamp in seconds
  const endTime = startTime + 143201; // use UNIX timestamp in seconds

  const remainingTime = endTime - startTime;
  const days = Math.ceil(remainingTime / daySeconds);
  const daysDuration = days * daySeconds;

  return (
    <section className="countdown">
      <div className="container">
        <div className="content d-flex">
          <h2 className="title lines">Canlı etkinliğin başlamasına...</h2>

          <div className="times d-flex">
            <div className="timebox">
              <div className="day mr-26">
                <CountdownCircleTimer
                  {...timerProps}
                  colors={[
                    ["#AD00FF", 0],
                    ["#0066FF", 1],
                  ]}
                  isLinearGradient={true}
                  duration={daysDuration}
                  initialRemainingTime={remainingTime}
                  trailColor={[["#FFFFFF"]]}
                >
                  {({ elapsedTime }) =>
                    renderTime(getTimeDays(daysDuration - elapsedTime))
                  }
                </CountdownCircleTimer>
              </div>
              <h3 className="title">gün</h3>
            </div>
            <div className="timebox">
              <div className="hours mr-26">
                <CountdownCircleTimer
                  {...timerProps}
                  colors={[
                    ["#AD00FF", 0],
                    ["#0066FF", 1],
                  ]}
                  isLinearGradient={true}
                  duration={daySeconds}
                  initialRemainingTime={remainingTime % daySeconds}
                  onComplete={(totalElapsedTime) => [
                    remainingTime - totalElapsedTime > hourSeconds,
                  ]}
                  trailColor={[["#FFFFFF"]]}
                >
                  {({ elapsedTime }) =>
                    renderTime(getTimeHours(daySeconds - elapsedTime))
                  }
                </CountdownCircleTimer>
              </div>
              <h3 className="title">saat</h3>
            </div>
            <div className="timebox">
              <div className="minutes mr-26">
                <CountdownCircleTimer
                  {...timerProps}
                  colors={[
                    ["#AD00FF", 0],
                    ["#0066FF", 1],
                  ]}
                  isLinearGradient={true}
                  duration={hourSeconds}
                  initialRemainingTime={remainingTime % hourSeconds}
                  onComplete={(totalElapsedTime) => [
                    remainingTime - totalElapsedTime > minuteSeconds,
                  ]}
                  trailColor={[["#FFFFFF"]]}
                >
                  {({ elapsedTime }) =>
                    renderTime(getTimeMinutes(hourSeconds - elapsedTime))
                  }
                </CountdownCircleTimer>
              </div>
              <h3 className="title">dakika</h3>
            </div>
            <div className="timebox">
              <div className="seconds mr-26">
                <CountdownCircleTimer
                  {...timerProps}
                  colors={[
                    ["#AD00FF", 0],
                    ["#0066FF", 1],
                  ]}
                  isLinearGradient={true}
                  duration={minuteSeconds}
                  initialRemainingTime={remainingTime % minuteSeconds}
                  onComplete={(totalElapsedTime) => [
                    remainingTime - totalElapsedTime > 0,
                  ]}
                >
                  {({ elapsedTime }) => renderTime(getTimeSeconds(elapsedTime))}
                </CountdownCircleTimer>
              </div>
              <h3 className="title">saniye kaldı</h3>
            </div>
          </div>

          <div className="to-calendar">
            <h3 className="title r-time">
              <Clock
                ticking={true}
                timezone={"Europe/Istanbul"}
                format={" DD MMMM YYYY, HH:mm"}
              />
            </h3>
            <button className="btn style-add">Takviminize Ekleyin</button>
          </div>
        </div>
      </div>
    </section>
  );
}
