import SwiperCore, { Navigation, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
SwiperCore.use([Navigation, A11y]);

function index() {
  return (
    <div className="slider-hero">
      <Swiper
        spaceBetween={0}
        slidesPerView={1}
        navigation
        loop
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
      >
        <SwiperSlide>
          <h4>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            <span>Tincidunt augue?</span>
          </h4>
        </SwiperSlide>
        <SwiperSlide>
          <h4>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            <span>Tincidunt augue?</span>
          </h4>
        </SwiperSlide>
        <SwiperSlide>
          <h4>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            <span>Tincidunt augue?</span>
          </h4>
        </SwiperSlide>
      </Swiper>
    </div>
  );
}

export default index;
