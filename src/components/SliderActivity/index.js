import SwiperCore, { Navigation, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
SwiperCore.use([Navigation, A11y]);

function index() {
  return (
    <section className="slider-activity">
      <div className="container">
        <Swiper
          spaceBetween={0}
          slidesPerView={5}
          navigation
          loop
          onSlideChange={() => console.log("slide change")}
          onSwiper={(swiper) => console.log(swiper)}
        >
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Welcome To Event</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">14:00</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Konuşmacı Sunumları</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">14:20</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Soru ve Cevaplar</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">14:40</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Son Değerlendirme</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">15:00</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Kapanış</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">15:20</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Açılış</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">15:40</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Değerlendirme</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">16:00</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Değerlendirme</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">16:20</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Değerlendirme</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">16:40</p>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div className="slide-content">
              <h5 className="title activity-head">Değerlendirme</h5>
              <h6 className="title activity-head date">16 Ocak 2021</h6>
              <p className="title activity-head activity-time">17:00</p>
            </div>
          </SwiperSlide>
        </Swiper>
      </div>
    </section>
  );
}

export default index;
