import "./App.scss";

//HTML
import Header from "./components/Header/index";
import SliderHero from "./components/SliderHero/index";
import RateIt from "./components/RateIt/index";
import Countdown from "./components/Countdown/index";
import AboutA from "./components/AboutA/index";
import SliderActivity from "./components/SliderActivity/index";
import QuestionComment from "./components/QuestionComment/index";
import Footer from "./components/Footer/index";

//STYLES
import "./components/Header/styles.scss";
import "./components/SliderHero/styles.scss";
import "./components/RateIt/styles.scss";
import "./components/Countdown/styles.scss";
import "./components/AboutA/styles.scss";
import "./components/SliderActivity/styles.scss";
import "./components/QuestionComment/styles.scss";
import "./components/Footer/styles.scss";

//Slider
import "swiper/swiper.scss";
import "swiper/components/navigation/navigation.scss";
import "react-rangeslider/lib/index.css";
import "react-circular-progressbar/dist/styles.css";

function App() {
  return (
    <div className="App">
      <Header />
      <SliderHero />
      <RateIt />
      <Countdown />
      <AboutA />
      <SliderActivity />
      <QuestionComment />
      <Footer />
    </div>
  );
}

export default App;
